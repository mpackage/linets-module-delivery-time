define([
    'ko',
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'domReady!',
], function(ko, $, Component, quote, storage, errorProcessor) {
    'use strict';

    return Component.extend({
        items: ko.observable(0),
        defaults: {
            template: 'Linets_DeliveryTime/checkout/shipping/delivery-time'
        },
        getTitle : function() {
            return "My Function is here";
        },

        initialize: function () {
            self = this;
            quote.shippingAddress.subscribe(this.shippingAddressObserver.bind(this));
            this._super();
        },

        isActive : function(){
            return true;
        },

        shippingAddressObserver: function (address) {
            var regionId = quote.shippingAddress().regionId;
            if (!this.isEmpty(regionId)) {
                var serviceUrl = 'rest/V1/linets-deliverytime/get-time?regionId=' + regionId + '&itemCount=' + window.checkoutConfig.quoteItemData.length;
                //var serviceUrl = 'rest/V1/linets-deliverytime/get-time?regionId=' + regionId;
                var payload = JSON.stringify({});
                this.getRate(payload, serviceUrl);
            }
        },

        loadItems: function(varItems) {
          varItems.forEach(element => {
              element.id_input = element.shipping_method + '_arrive_id';
          })
            self.items(varItems);
        },

        getRate: function (payload,serviceUrl) {
            storage.post(
                serviceUrl, payload, false
            ).done(
                function (result) {
                    setTimeout(function () {
                        result.forEach(element => {
                            $('#label_carrier_' + element.shipping_method).html($('#label_carrier_' + element.shipping_method).html() + ' available days ' + element.delivery_day + ' arrive: ' + element.arrives)
                        });
                    }, 2500);
                    self.loadItems(result);
                }
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                }
            );
        },
        isEmpty: function(str) {
            return (!str || str.length === 0 );
        },
    });
});
