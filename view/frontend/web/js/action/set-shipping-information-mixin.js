/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            var selector, selector_days, selector_promise = '';
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }
            selector = '#' + $("co-shipping-method-form input[type='radio']:checked").val();
            selector_days = selector + '_delivery_day';
            selector_promise = selector + '_promise_day';
            shippingAddress['extension_attributes']['avaibledays'] = $(selector_days).val();
            shippingAddress['extension_attributes']['promise'] = $(selector_promise).val();
            return originalAction();
        });
    };
});

