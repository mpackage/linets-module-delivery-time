define([
    'underscore',
    'Magento_Ui/js/grid/columns/select'
], function (_, Column) {
    'use strict';
    return Column.extend({
        defaults: {
            bodyTmpl: 'Linets_GriftWrap/ui/grid/cells/promise'
        },
        getPromise : function (row) {
            return row.promise;
        }
    })
})
