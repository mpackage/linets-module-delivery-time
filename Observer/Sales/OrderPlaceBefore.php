<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Observer\Sales;

class OrderPlaceBefore implements \Magento\Framework\Event\ObserverInterface
{

    /**
     * @var \Linets\DeliveryTime\Helper\Data
     */
    protected $_helper;

    /**
     * OrderPlaceBefore constructor.
     * @param \Linets\DeliveryTime\Helper\Data $data
     */
    public function __construct(
        \Linets\DeliveryTime\Helper\Data $data
    )
    {
        $this->_helper = $data;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        $order = $observer->getEvent()->getOrder();
        $address = $order->getShippingAddress();
        $shippingMethod = $address->getShippingMethod();
        $regionId = $address->getRegionId();
        $itemCount = count($order->getItems());
        $this->_helper->getPromise($shippingMethod, $regionId, $itemCount);

    }
}
