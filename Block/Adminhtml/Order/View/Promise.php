<?php


namespace Linets\DeliveryTime\Block\Adminhtml\Order\View;


class Promise extends \Magento\Backend\Block\Template
{

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }


    public function getPromise()
    {
        $order = $this->_coreRegistry->registry('current_order');
        return [
            'date' => $order->getExtensionAttributes()->getDate(),
            'days' => $order->getExtensionAttributes()->getDays()
        ];
    }
}

