<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Controller\Adminhtml\DeliveryTime;

class Edit extends \Linets\DeliveryTime\Controller\Adminhtml\DeliveryTime
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('deliverytime_id');
        $model = $this->_objectManager->create(\Linets\DeliveryTime\Model\DeliveryTime::class);
        
        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Deliverytime no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }
        $this->_coreRegistry->register('linets_deliverytime_deliverytime', $model);
        
        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Deliverytime') : __('New Deliverytime'),
            $id ? __('Edit Deliverytime') : __('New Deliverytime')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Deliverytimes'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Deliverytime %1', $model->getId()) : __('New Deliverytime'));
        return $resultPage;
    }
}

