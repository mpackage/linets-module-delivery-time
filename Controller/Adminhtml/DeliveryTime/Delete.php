<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Controller\Adminhtml\DeliveryTime;

class Delete extends \Linets\DeliveryTime\Controller\Adminhtml\DeliveryTime
{

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('deliverytime_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Linets\DeliveryTime\Model\DeliveryTime::class);
                $model->load($id);
                $model->delete();
                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Deliverytime.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['deliverytime_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Deliverytime to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}

