<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Controller\Adminhtml\DeliveryTime;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('deliverytime_id');

            $model = $this->_objectManager->create(\Linets\DeliveryTime\Model\DeliveryTime::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Deliverytime no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $data['storeView']= implode(',',$data['storeView']);
            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Deliverytime.'));
                $this->dataPersistor->clear('linets_deliverytime_deliverytime');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['deliverytime_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Deliverytime.'));
            }

            $this->dataPersistor->set('linets_deliverytime_deliverytime', $data);
            return $resultRedirect->setPath('*/*/edit', ['deliverytime_id' => $this->getRequest()->getParam('deliverytime_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}

