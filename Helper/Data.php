<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    const DEFAULT_DAYS = 'delivery_days/general/dafault_days';

    /**
     * @var \Linets\DeliveryTime\Model\Data\DeliveryTimeOutputFactory
     */
    protected $_deliveryTimeOutputFactory;

    /**
     * @var \Linets\DeliveryTime\Model\DeliveryTimeFactory
     */
    protected $_deliveryTimeFactory;

    /**
     * @var \Linets\DeliveryTime\Model\HolidayFactory
     */
    protected $_holidayTimeFactory;

    /**
     * Data constructor.
     * @param \Linets\DeliveryTime\Model\Data\DeliveryTimeOutputFactory $deliveryTimeOutputFactory
     * @param \Linets\DeliveryTime\Model\DeliveryTimeFactory $deliveryTimeFactory
     * @param \Linets\DeliveryTime\Model\HolidayFactory $holidayTimeFactory
     * @param Context $context
     */
    public function __construct(
        \Linets\DeliveryTime\Model\Data\DeliveryTimeOutputFactory $deliveryTimeOutputFactory,
        \Linets\DeliveryTime\Model\DeliveryTimeFactory $deliveryTimeFactory,
        \Linets\DeliveryTime\Model\HoliDayFactory $holidayTimeFactory,
        Context $context
    )
    {
        $this->_deliveryTimeOutputFactory = $deliveryTimeOutputFactory;
        $this->_deliveryTimeFactory = $deliveryTimeFactory;
        $this->_holidayTimeFactory = $holidayTimeFactory;
        $this->_scopeConfig = $context->getScopeConfig();
        parent::__construct($context);
    }

    public function getListShippingMethodByRegion($regionId, $itemCount = 1, $store = 0)
    {
        $outShipping = [];
        $deliveryTime = $this->_deliveryTimeFactory->create();
        $deliveryTime = $deliveryTime->getCollection()
            ->addFieldToFilter('regionId', ['eq' => $regionId])
            ->addFieldToFilter('storeView', ['in' => [0, $store]]);
        foreach ($deliveryTime as $shipping)
        {
            $day = $itemCount == 1 ? $shipping->getDay() : $shipping->getDay2();
            $arrive = $this->getArribesWithHoliDay($shipping->getDay(), 0);
            $outShipping[] = $this->createOut(
                $shipping->getData('shippingMethod'),
                (int)$day,
                $arrive
            );
        }
        return $outShipping;
    }

    public function createOut(
        string $shippingMethod,
        int $deliveryDay,
        string $arrive) : \Linets\DeliveryTime\Model\Data\DeliveryTimeOutput
    {
        $delivery = $this->_deliveryTimeOutputFactory->create();
        $delivery->setShippingMethod($shippingMethod);
        $delivery->setDeliveryDay($deliveryDay);
        $delivery->setArrives($arrive);
        return $delivery;
    }

    public function getArribesWithHoliDay($day, $store) {
        $today = date("Y-m-d");
        $toTimestamp = strtotime("+ $day day");
        $toDate = date("Y-m-d", $toTimestamp);
        $holiday = $this->_holidayTimeFactory->create();
        $moreDays = $holiday->getCollection()
            ->addFieldToFilter('date', ['from' => $today, 'to' => $toDate])
            ->getSize();
        $totalDay = $day + $moreDays;
        $toTimestamp = strtotime("+ $totalDay day");
        $toDate = date("Y-m-d", $toTimestamp);

        // Add sunday days
        $start = strtotime($today);
        $end   = strtotime($toDate);
        $sundays = 0;
        for ($i = $start; $i <= $end; $i = strtotime("+1 day", $i)) {
            $day = date("w", $i);  // 0=sun, 1=mon, ..., 6=sat
            //  SUNDAY = 0
            if ($day == 0) {
                $sundays++;
            }
        }
        $totalDay = $totalDay + $sundays;
        $toTimestamp = strtotime("+ $totalDay day");
        $toDate = date("Y-m-d", $toTimestamp);

        return $toDate;
    }

    public function getPromise($order, &$deliveryDay, &$arrives)
    {
        $deliveryDay = $this->getConfig(self::DEFAULT_DAYS);
        $arrives = date("Y-m-d");
        $regionId = $order->getShippingAddress()->getRegionId();
        $store = $order->getStoreId();
        $itemsCount = count($order->getItems());
        $shippingMethod = $order->getShippingMethod();
        $listPromise = $this->getListShippingMethodByRegion($regionId, $itemsCount, $store);
        foreach ($listPromise as $promise)
        {
            if ($promise->getShippingMethod() == $shippingMethod) {
                $deliveryDay = $promise->getDeliveryDay();
                $arrives = $promise->getArrives();
            }
        }
    }

    /**
     * Get config to path
     * @param $config_path
     * @return mixed
     */
    public function getConfig($config_path)
    {
        return $this->_scopeConfig->getValue(
            $config_path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}
