<?php

namespace Linets\DeliveryTime\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\App\ResourceConnection;

class RegionCl implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /**
     * @var ResourceConnection
     */
    protected $_resourceConnection;

    /**
     * AddEnableColorAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param EavSetupFactory $eavSetupFactory
     * @param ResourceConnection $resourceConnection
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory,
        ResourceConnection $resourceConnection
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->_resourceConnection = $resourceConnection;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        // set up array containing regions to be added (region_code => region_name)
        $new_regions = array(
            'AP' => 'XV - Arica y Parinacota',
            'TA' => 'I - Tarapacá',
            'AN' => 'II - Antofagasta',
            'AT' => 'III - Atacama',
            'CO' => 'IV - Coquimbo',
            'VA' => 'V - Valparaíso',
            'RM' => 'RM - Región Metropolitana',
            'LI' => 'VI - OHiggigns',
            'ML' => 'VII - Maule',
            'BI' => 'VIII - Biobío',
            'AR' => 'IX - La Araucanía',
            'LR' => 'XIV - Los Ríos',
            'LL' => 'X - Los Lagos',
            'AY' => 'XI - Aysén',
            'MA' => 'XII - Magallanes',
            'NB' => 'XVI - Ñuble'
        );
        // specify country code for new regions
        $country_code = 'CL';

        // specify locale
        $locale = 'es_CL';

        // create our core_write conection object
        //$connection =  Mage::getSingleton('core/resource')->getConnection('core_write');
        $connection = $this->_resourceConnection->getConnection();
        // iterate our new regions
        foreach ($new_regions as $region_code => $region_name) {

            // insert region
            $sql = "INSERT INTO `directory_country_region` (`region_id`,`country_id`,`code`,`default_name`) VALUES (NULL,?,?,?)";
            $connection->query($sql,array($country_code,$region_code,$region_name));

            // get new region id for next query
            $region_id = $connection->lastInsertId();

            // insert region name
            $sql = "INSERT INTO `directory_country_region_name` (`locale`,`region_id`,`name`) VALUES (?,?,?)";
            $connection->query($sql,array($locale,$region_id,$region_name));
        }    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getVersion()
    {
        return '1.0.0';
    }
}
