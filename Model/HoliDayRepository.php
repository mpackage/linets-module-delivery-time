<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model;

use Linets\DeliveryTime\Api\Data\HoliDayInterfaceFactory;
use Linets\DeliveryTime\Api\Data\HoliDaySearchResultsInterfaceFactory;
use Linets\DeliveryTime\Api\HoliDayRepositoryInterface;
use Linets\DeliveryTime\Model\ResourceModel\HoliDay as ResourceHoliDay;
use Linets\DeliveryTime\Model\ResourceModel\HoliDay\CollectionFactory as HoliDayCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class HoliDayRepository implements HoliDayRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $dataHoliDayFactory;

    protected $searchResultsFactory;

    protected $holiDayFactory;

    protected $holiDayCollectionFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceHoliDay $resource
     * @param HoliDayFactory $holiDayFactory
     * @param HoliDayInterfaceFactory $dataHoliDayFactory
     * @param HoliDayCollectionFactory $holiDayCollectionFactory
     * @param HoliDaySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceHoliDay $resource,
        HoliDayFactory $holiDayFactory,
        HoliDayInterfaceFactory $dataHoliDayFactory,
        HoliDayCollectionFactory $holiDayCollectionFactory,
        HoliDaySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->holiDayFactory = $holiDayFactory;
        $this->holiDayCollectionFactory = $holiDayCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataHoliDayFactory = $dataHoliDayFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Linets\DeliveryTime\Api\Data\HoliDayInterface $holiDay
    ) {
        /* if (empty($holiDay->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $holiDay->setStoreId($storeId);
        } */
        
        $holiDayData = $this->extensibleDataObjectConverter->toNestedArray(
            $holiDay,
            [],
            \Linets\DeliveryTime\Api\Data\HoliDayInterface::class
        );
        
        $holiDayModel = $this->holiDayFactory->create()->setData($holiDayData);
        
        try {
            $this->resource->save($holiDayModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the holiDay: %1',
                $exception->getMessage()
            ));
        }
        return $holiDayModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($holiDayId)
    {
        $holiDay = $this->holiDayFactory->create();
        $this->resource->load($holiDay, $holiDayId);
        if (!$holiDay->getId()) {
            throw new NoSuchEntityException(__('holiDay with id "%1" does not exist.', $holiDayId));
        }
        return $holiDay->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->holiDayCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Linets\DeliveryTime\Api\Data\HoliDayInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Linets\DeliveryTime\Api\Data\HoliDayInterface $holiDay
    ) {
        try {
            $holiDayModel = $this->holiDayFactory->create();
            $this->resource->load($holiDayModel, $holiDay->getHolidayId());
            $this->resource->delete($holiDayModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the holiDay: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($holiDayId)
    {
        return $this->delete($this->get($holiDayId));
    }
}

