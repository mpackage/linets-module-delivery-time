<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model;

use Linets\DeliveryTime\Api\Data\PromiseInterface;
use Linets\DeliveryTime\Api\Data\PromiseInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Promise extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'linets_deliverytime_promise';
    protected $dataObjectHelper;

    protected $promiseDataFactory;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param PromiseInterfaceFactory $promiseDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Linets\DeliveryTime\Model\ResourceModel\Promise $resource
     * @param \Linets\DeliveryTime\Model\ResourceModel\Promise\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        PromiseInterfaceFactory $promiseDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Linets\DeliveryTime\Model\ResourceModel\Promise $resource,
        \Linets\DeliveryTime\Model\ResourceModel\Promise\Collection $resourceCollection,
        array $data = []
    ) {
        $this->promiseDataFactory = $promiseDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve promise model with promise data
     * @return PromiseInterface
     */
    public function getDataModel()
    {
        $promiseData = $this->getData();

        $promiseDataObject = $this->promiseDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $promiseDataObject,
            $promiseData,
            PromiseInterface::class
        );

        return $promiseDataObject;
    }
}
