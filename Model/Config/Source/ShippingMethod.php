<?php
namespace Linets\Deliverytime\Model\Config\Source;

class ShippingMethod implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Magento\Shipping\Model\Config\Source\Allmethods
     */
    protected $_shippingAllmethods;

    /**
     * ShippingMethod constructor.
     * @param \Magento\Shipping\Model\Config\Source\Allmethods $shippingAllmethods
     */
    public function __construct(
        \Magento\Shipping\Model\Config\Source\Allmethods $shippingAllmethods
    ) {
        $this->_shippingAllmethods = $shippingAllmethods;
    }

    public function toOptionArray()
    {
        $shippingAllMethod = $this->_shippingAllmethods->toOptionArray(true);

        foreach ($shippingAllMethod as $shippingMethod) {

            $options[] = [
                "value" => $shippingMethod['value'],
                "label" => $shippingMethod['label']
            ];
        }

        return $options;
    }
}
