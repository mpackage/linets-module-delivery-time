<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model;

use Linets\DeliveryTime\Api\Data\HoliDayInterface;
use Linets\DeliveryTime\Api\Data\HoliDayInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class HoliDay extends \Magento\Framework\Model\AbstractModel
{

    protected $holidayDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'linets_deliverytime_holiday';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param HoliDayInterfaceFactory $holidayDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Linets\DeliveryTime\Model\ResourceModel\HoliDay $resource
     * @param \Linets\DeliveryTime\Model\ResourceModel\HoliDay\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        HoliDayInterfaceFactory $holidayDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Linets\DeliveryTime\Model\ResourceModel\HoliDay $resource,
        \Linets\DeliveryTime\Model\ResourceModel\HoliDay\Collection $resourceCollection,
        array $data = []
    ) {
        $this->holidayDataFactory = $holidayDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve holiday model with holiday data
     * @return HoliDayInterface
     */
    public function getDataModel()
    {
        $holidayData = $this->getData();
        
        $holidayDataObject = $this->holidayDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $holidayDataObject,
            $holidayData,
            HoliDayInterface::class
        );
        
        return $holidayDataObject;
    }
}

