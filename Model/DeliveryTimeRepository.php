<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model;

use Linets\DeliveryTime\Api\Data\DeliveryTimeInterfaceFactory;
use Linets\DeliveryTime\Api\Data\DeliveryTimeSearchResultsInterfaceFactory;
use Linets\DeliveryTime\Api\DeliveryTimeRepositoryInterface;
use Linets\DeliveryTime\Model\ResourceModel\DeliveryTime as ResourceDeliveryTime;
use Linets\DeliveryTime\Model\ResourceModel\DeliveryTime\CollectionFactory as DeliveryTimeCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class DeliveryTimeRepository implements DeliveryTimeRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    private $storeManager;

    protected $dataDeliveryTimeFactory;

    protected $dataObjectHelper;

    protected $deliveryTimeFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $deliveryTimeCollectionFactory;


    /**
     * @param ResourceDeliveryTime $resource
     * @param DeliveryTimeFactory $deliveryTimeFactory
     * @param DeliveryTimeInterfaceFactory $dataDeliveryTimeFactory
     * @param DeliveryTimeCollectionFactory $deliveryTimeCollectionFactory
     * @param DeliveryTimeSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceDeliveryTime $resource,
        DeliveryTimeFactory $deliveryTimeFactory,
        DeliveryTimeInterfaceFactory $dataDeliveryTimeFactory,
        DeliveryTimeCollectionFactory $deliveryTimeCollectionFactory,
        DeliveryTimeSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->deliveryTimeFactory = $deliveryTimeFactory;
        $this->deliveryTimeCollectionFactory = $deliveryTimeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataDeliveryTimeFactory = $dataDeliveryTimeFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface $deliveryTime
    ) {
        /* if (empty($deliveryTime->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $deliveryTime->setStoreId($storeId);
        } */
        
        $deliveryTimeData = $this->extensibleDataObjectConverter->toNestedArray(
            $deliveryTime,
            [],
            \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface::class
        );
        
        $deliveryTimeModel = $this->deliveryTimeFactory->create()->setData($deliveryTimeData);
        
        try {
            $this->resource->save($deliveryTimeModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the deliveryTime: %1',
                $exception->getMessage()
            ));
        }
        return $deliveryTimeModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($deliveryTimeId)
    {
        $deliveryTime = $this->deliveryTimeFactory->create();
        $this->resource->load($deliveryTime, $deliveryTimeId);
        if (!$deliveryTime->getId()) {
            throw new NoSuchEntityException(__('deliveryTime with id "%1" does not exist.', $deliveryTimeId));
        }
        return $deliveryTime->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->deliveryTimeCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface $deliveryTime
    ) {
        try {
            $deliveryTimeModel = $this->deliveryTimeFactory->create();
            $this->resource->load($deliveryTimeModel, $deliveryTime->getDeliverytimeId());
            $this->resource->delete($deliveryTimeModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the deliveryTime: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($deliveryTimeId)
    {
        return $this->delete($this->get($deliveryTimeId));
    }
}

