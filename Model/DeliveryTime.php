<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model;

use Linets\DeliveryTime\Api\Data\DeliveryTimeInterface;
use Linets\DeliveryTime\Api\Data\DeliveryTimeInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class DeliveryTime extends \Magento\Framework\Model\AbstractModel
{

    protected $deliverytimeDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'linets_deliverytime_deliverytime';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DeliveryTimeInterfaceFactory $deliverytimeDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Linets\DeliveryTime\Model\ResourceModel\DeliveryTime $resource
     * @param \Linets\DeliveryTime\Model\ResourceModel\DeliveryTime\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        DeliveryTimeInterfaceFactory $deliverytimeDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Linets\DeliveryTime\Model\ResourceModel\DeliveryTime $resource,
        \Linets\DeliveryTime\Model\ResourceModel\DeliveryTime\Collection $resourceCollection,
        array $data = []
    ) {
        $this->deliverytimeDataFactory = $deliverytimeDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve deliverytime model with deliverytime data
     * @return DeliveryTimeInterface
     */
    public function getDataModel()
    {
        $deliverytimeData = $this->getData();
        
        $deliverytimeDataObject = $this->deliverytimeDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $deliverytimeDataObject,
            $deliverytimeData,
            DeliveryTimeInterface::class
        );
        
        return $deliverytimeDataObject;
    }
}

