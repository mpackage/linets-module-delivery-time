<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model;

use Linets\DeliveryTime\Api\Data\PromiseInterfaceFactory;
use Linets\DeliveryTime\Api\Data\PromiseSearchResultsInterfaceFactory;
use Linets\DeliveryTime\Api\PromiseRepositoryInterface;
use Linets\DeliveryTime\Model\ResourceModel\Promise as ResourcePromise;
use Linets\DeliveryTime\Model\ResourceModel\Promise\CollectionFactory as PromiseCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class PromiseRepository implements PromiseRepositoryInterface
{

    protected $resource;

    protected $extensibleDataObjectConverter;
    protected $searchResultsFactory;

    protected $promiseFactory;

    protected $dataPromiseFactory;

    protected $promiseCollectionFactory;

    private $storeManager;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourcePromise $resource
     * @param PromiseFactory $promiseFactory
     * @param PromiseInterfaceFactory $dataPromiseFactory
     * @param PromiseCollectionFactory $promiseCollectionFactory
     * @param PromiseSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourcePromise $resource,
        PromiseFactory $promiseFactory,
        PromiseInterfaceFactory $dataPromiseFactory,
        PromiseCollectionFactory $promiseCollectionFactory,
        PromiseSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->promiseFactory = $promiseFactory;
        $this->promiseCollectionFactory = $promiseCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataPromiseFactory = $dataPromiseFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Linets\DeliveryTime\Api\Data\PromiseInterface $promise
    ) {
        /* if (empty($promise->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $promise->setStoreId($storeId);
        } */

        $promiseData = $this->extensibleDataObjectConverter->toNestedArray(
            $promise,
            [],
            \Linets\DeliveryTime\Api\Data\PromiseInterface::class
        );

        $promiseModel = $this->promiseFactory->create()->setData($promiseData);

        try {
            $this->resource->save($promiseModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the promise: %1',
                $exception->getMessage()
            ));
        }
        return $promiseModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($promiseId)
    {
        $promise = $this->promiseFactory->create();
        $this->resource->load($promise, $promiseId);
        if (!$promise->getId()) {
            throw new NoSuchEntityException(__('Promise with id "%1" does not exist.', $promiseId));
        }
        return $promise->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->promiseCollectionFactory->create();

        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Linets\DeliveryTime\Api\Data\PromiseInterface::class
        );

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Linets\DeliveryTime\Api\Data\PromiseInterface $promise
    ) {
        try {
            $promiseModel = $this->promiseFactory->create();
            $this->resource->load($promiseModel, $promise->getPromiseId());
            $this->resource->delete($promiseModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Promise: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($promiseId)
    {
        return $this->delete($this->get($promiseId));
    }
}
