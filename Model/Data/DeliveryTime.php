<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model\Data;

use Linets\DeliveryTime\Api\Data\DeliveryTimeInterface;

class DeliveryTime extends \Magento\Framework\Api\AbstractExtensibleObject implements DeliveryTimeInterface
{

    /**
     * Get deliverytime_id
     * @return string|null
     */
    public function getDeliverytimeId()
    {
        return $this->_get(self::DELIVERYTIME_ID);
    }

    /**
     * Set deliverytime_id
     * @param string $deliverytimeId
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setDeliverytimeId($deliverytimeId)
    {
        return $this->setData(self::DELIVERYTIME_ID, $deliverytimeId);
    }

    /**
     * Get storeView
     * @return string|null
     */
    public function getStoreView()
    {
        return $this->_get(self::STOREVIEW);
    }

    /**
     * Set storeView
     * @param string $storeView
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setStoreView($storeView)
    {
        return $this->setData(self::STOREVIEW, $storeView);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Linets\DeliveryTime\Api\Data\DeliveryTimeExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\DeliveryTime\Api\Data\DeliveryTimeExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get regionId
     * @return string|null
     */
    public function getRegionId()
    {
        return $this->_get(self::REGIONID);
    }

    /**
     * Set regionId
     * @param string $regionId
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setRegionId($regionId)
    {
        return $this->setData(self::REGIONID, $regionId);
    }

    /**
     * Get day
     * @return string|null
     */
    public function getDay()
    {
        return $this->_get(self::DAY);
    }

    /**
     * Set day
     * @param string $day
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setDay($day)
    {
        return $this->setData(self::DAY, $day);
    }

    /**
     * Get day2
     * @return string|null
     */
    public function getDay2()
    {
        return $this->_get(self::DAY2);
    }

    /**
     * Set day2
     * @param string $day2
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setDay2($day2)
    {
        return $this->setData(self::DAY2, $day2);
    }

    /**
     * Get shippingMethod
     * @return string|null
     */
    public function getShippingMethod()
    {
        return $this->_get(self::SHIPPINGMETHOD);
    }

    /**
     * Set shippingMethod
     * @param string $shipMethod
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setShippingMethod($shipMethod)
    {
        return $this->setData(self::SHIPPINGMETHOD, $shipMethod);
    }
}

