<?php
namespace Linets\DeliveryTime\Model\Data;

use Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface;

class DeliveryTimeOutput extends \Magento\Framework\Api\AbstractExtensibleObject implements DeliveryTimeOutputInterface {

    /**
     * Get delivery day
     * @return string
     */
    public function getShippingMethod() {
        return $this->_get(self::SHIPPING_METHOD);
    }

    /**
     * Set Shipping Method
     * @param string $shippingMethod
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface
     */
    public function setShippingMethod($shippingMethod)
    {
        return $this->setData(self::SHIPPING_METHOD, $shippingMethod);
    }

    /**
     * Get delivery day
     * @return string[]
     */
    public function getDeliveryDay() {
        return $this->_get(self::DELIVERY_DAY);
    }

    /**
     * Set Day
     * @param string $day
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface
     */
    public function setDeliveryDay($day)
    {
        return $this->setData(self::DELIVERY_DAY, $day);
    }
    /**
     * Get arrives
     * @return string[]
     */
    public function getArrives() {
        return $this->_get(self::ARRIVES);
    }

    /**
     * Set Arrives
     * @param string $arrives
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface
     */
    public function setArrives($arrives)
    {
        return $this->setData(self::ARRIVES, $arrives);
    }

}
