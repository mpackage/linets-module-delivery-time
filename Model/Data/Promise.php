<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model\Data;

use Linets\DeliveryTime\Api\Data\PromiseInterface;

class Promise extends \Magento\Framework\Api\AbstractExtensibleObject implements PromiseInterface
{

    /**
     * Get promise_id
     * @return string|null
     */
    public function getPromiseId()
    {
        return $this->_get(self::PROMISE_ID);
    }

    /**
     * Set promise_id
     * @param string $promiseId
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setPromiseId($promiseId)
    {
        return $this->setData(self::PROMISE_ID, $promiseId);
    }

    /**
     * Get order
     * @return string|null
     */
    public function getOrder()
    {
        return $this->_get(self::ORDER);
    }

    /**
     * Set order
     * @param string $order
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setOrder($order)
    {
        return $this->setData(self::ORDER, $order);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\DeliveryTime\Api\Data\PromiseExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Linets\DeliveryTime\Api\Data\PromiseExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\DeliveryTime\Api\Data\PromiseExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get days
     * @return string|null
     */
    public function getDays()
    {
        return $this->_get(self::DAYS);
    }

    /**
     * Set days
     * @param string $days
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setDays($days)
    {
        return $this->setData(self::DAYS, $days);
    }

    /**
     * Get date
     * @return string|null
     */
    public function getDate()
    {
        return $this->_get(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }
}
