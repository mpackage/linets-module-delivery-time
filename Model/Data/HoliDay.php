<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model\Data;

use Linets\DeliveryTime\Api\Data\HoliDayInterface;

class HoliDay extends \Magento\Framework\Api\AbstractExtensibleObject implements HoliDayInterface
{

    /**
     * Get holiday_id
     * @return string|null
     */
    public function getHolidayId()
    {
        return $this->_get(self::HOLIDAY_ID);
    }

    /**
     * Set holiday_id
     * @param string $holidayId
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setHolidayId($holidayId)
    {
        return $this->setData(self::HOLIDAY_ID, $holidayId);
    }

    /**
     * Get storeView
     * @return string|null
     */
    public function getStoreView()
    {
        return $this->_get(self::STOREVIEW);
    }

    /**
     * Set storeView
     * @param string $storeView
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setStoreView($storeView)
    {
        return $this->setData(self::STOREVIEW, $storeView);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\DeliveryTime\Api\Data\HoliDayExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Linets\DeliveryTime\Api\Data\HoliDayExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\DeliveryTime\Api\Data\HoliDayExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get date
     * @return string|null
     */
    public function getDate()
    {
        return $this->_get(self::DATE);
    }

    /**
     * Set date
     * @param string $date
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setDate($date)
    {
        return $this->setData(self::DATE, $date);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }
}

