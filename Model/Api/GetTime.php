<?php
namespace Linets\DeliveryTime\Model\Api;

class GetTime implements \Linets\DeliveryTime\Api\GetTimeInterface {

    /**
     * @var \Linets\DeliveryTime\Helper\Data
     */
    protected $_helper;

    /**
     * GetTime constructor.
     * @param \Linets\DeliveryTime\Helper\Data $helper
     */
    public function __construct(
        \Linets\DeliveryTime\Helper\Data $helper
    )
    {
        $this->_helper = $helper;
    }

    /**
     * @return string[]
     **/
    public function getTime($regionId, $itemCount)
    {
        return $this->_helper->getListShippingMethodByRegion($regionId, $itemCount);
    }
}
