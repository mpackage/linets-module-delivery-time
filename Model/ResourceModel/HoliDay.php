<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model\ResourceModel;

class HoliDay extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('linets_deliverytime_holiday', 'holiday_id');
    }
}

