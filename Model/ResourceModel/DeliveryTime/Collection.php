<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Model\ResourceModel\DeliveryTime;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'deliverytime_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Linets\DeliveryTime\Model\DeliveryTime::class,
            \Linets\DeliveryTime\Model\ResourceModel\DeliveryTime::class
        );
    }
}

