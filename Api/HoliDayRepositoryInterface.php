<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface HoliDayRepositoryInterface
{

    /**
     * Save holiDay
     * @param \Linets\DeliveryTime\Api\Data\HoliDayInterface $holiDay
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Linets\DeliveryTime\Api\Data\HoliDayInterface $holiDay
    );

    /**
     * Retrieve holiDay
     * @param string $holidayId
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($holidayId);

    /**
     * Retrieve holiDay matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Linets\DeliveryTime\Api\Data\HoliDaySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete holiDay
     * @param \Linets\DeliveryTime\Api\Data\HoliDayInterface $holiDay
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Linets\DeliveryTime\Api\Data\HoliDayInterface $holiDay
    );

    /**
     * Delete holiDay by ID
     * @param string $holidayId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($holidayId);
}

