<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface PromiseRepositoryInterface
{

    /**
     * Save Promise
     * @param \Linets\DeliveryTime\Api\Data\PromiseInterface $promise
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Linets\DeliveryTime\Api\Data\PromiseInterface $promise
    );

    /**
     * Retrieve Promise
     * @param string $promiseId
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($promiseId);

    /**
     * Retrieve Promise matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Linets\DeliveryTime\Api\Data\PromiseSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Promise
     * @param \Linets\DeliveryTime\Api\Data\PromiseInterface $promise
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Linets\DeliveryTime\Api\Data\PromiseInterface $promise
    );

    /**
     * Delete Promise by ID
     * @param string $promiseId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($promiseId);
}
