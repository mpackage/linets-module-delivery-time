<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface DeliveryTimeOutputInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const SHIPPING_METHOD = 'shippingMethod';
    const DELIVERY_DAY = 'deliveryDay';
    const ARRIVES = 'arrives';

    /**
     * Get shipping
     * @return string | null
     */
    public function getShippingMethod();

    /**
     * Set shippingMethod
     * @param string $shippingMethod
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface
     */
    public function setShippingMethod($shippingMethod);

    /**
     * Get deliveryday
     * @return string | null
     */
    public function getDeliveryDay();

    /**
     * Set deliveryday
     * @param string $day
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface
     */
    public function setDeliveryDay($day);

    /**
     * Get arrives
     * @return string | null
     */
    public function getArrives();

    /**
     * Set arrives
     * @param string $arrives
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface
     */
    public function setArrives($arrives);
}

