<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface PromiseSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Promise list.
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface[]
     */
    public function getItems();

    /**
     * Set order list.
     * @param \Linets\DeliveryTime\Api\Data\PromiseInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
