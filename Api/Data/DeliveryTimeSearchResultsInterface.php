<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface DeliveryTimeSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get deliveryTime list.
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface[]
     */
    public function getItems();

    /**
     * Set storeView list.
     * @param \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

