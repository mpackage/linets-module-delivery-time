<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface DeliveryTimeInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const DELIVERYTIME_ID = 'deliverytime_id';
    const STOREVIEW = 'storeView';
    const DAY2 = 'day2';
    const REGIONID = 'regionId';
    const DAY = 'day';
    const SHIPPINGMETHOD = 'shippingMethod';

    /**
     * Get deliverytime_id
     * @return string|null
     */
    public function getDeliverytimeId();

    /**
     * Set deliverytime_id
     * @param string $deliverytimeId
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setDeliverytimeId($deliverytimeId);

    /**
     * Get storeView
     * @return string|null
     */
    public function getStoreView();

    /**
     * Set storeView
     * @param string $storeView
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setStoreView($storeView);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Linets\DeliveryTime\Api\Data\DeliveryTimeExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\DeliveryTime\Api\Data\DeliveryTimeExtensionInterface $extensionAttributes
    );

    /**
     * Get regionId
     * @return string|null
     */
    public function getRegionId();

    /**
     * Set regionId
     * @param string $regionId
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setRegionId($regionId);

    /**
     * Get day
     * @return string|null
     */
    public function getDay();

    /**
     * Set day
     * @param string $day
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setDay($day);

    /**
     * Get day2
     * @return string|null
     */
    public function getDay2();

    /**
     * Set day2
     * @param string $day2
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setDay2($day2);

    /**
     * Get shippingMethod
     * @return string|null
     */
    public function getShippingMethod();

    /**
     * Set shippingMethod
     * @param string $shippingMethod
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     */
    public function setShippingMethod($shippingMethod);
}

