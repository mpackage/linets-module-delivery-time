<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface HoliDaySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get holiDay list.
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface[]
     */
    public function getItems();

    /**
     * Set storeView list.
     * @param \Linets\DeliveryTime\Api\Data\HoliDayInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

