<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface PromiseInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const DATE = 'date';
    const ORDER = 'order';
    const DAYS = 'days';
    const PROMISE_ID = 'promise_id';

    /**
     * Get promise_id
     * @return string|null
     */
    public function getPromiseId();

    /**
     * Set promise_id
     * @param string $promiseId
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setPromiseId($promiseId);

    /**
     * Get order
     * @return string|null
     */
    public function getOrder();

    /**
     * Set order
     * @param string $order
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setOrder($order);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\DeliveryTime\Api\Data\PromiseExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Linets\DeliveryTime\Api\Data\PromiseExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\DeliveryTime\Api\Data\PromiseExtensionInterface $extensionAttributes
    );

    /**
     * Get days
     * @return string|null
     */
    public function getDays();

    /**
     * Set days
     * @param string $days
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setDays($days);

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Linets\DeliveryTime\Api\Data\PromiseInterface
     */
    public function setDate($date);
}
