<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api\Data;

interface HoliDayInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const HOLIDAY_ID = 'holiday_id';
    const DATE = 'date';
    const STOREVIEW = 'storeView';
    const NAME = 'name';

    /**
     * Get holiday_id
     * @return string|null
     */
    public function getHolidayId();

    /**
     * Set holiday_id
     * @param string $holidayId
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setHolidayId($holidayId);

    /**
     * Get storeView
     * @return string|null
     */
    public function getStoreView();

    /**
     * Set storeView
     * @param string $storeView
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setStoreView($storeView);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Linets\DeliveryTime\Api\Data\HoliDayExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Linets\DeliveryTime\Api\Data\HoliDayExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Linets\DeliveryTime\Api\Data\HoliDayExtensionInterface $extensionAttributes
    );

    /**
     * Get date
     * @return string|null
     */
    public function getDate();

    /**
     * Set date
     * @param string $date
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setDate($date);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Linets\DeliveryTime\Api\Data\HoliDayInterface
     */
    public function setName($name);
}

