<?php
namespace Linets\DeliveryTime\Api;

interface GetTimeInterface
{

    /**
     * Get for Post api
     * @param string $regionId
     * @param string $itemCount
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeOutputInterface[]
     */
    public function getTime($regionId, $itemCount);
}
