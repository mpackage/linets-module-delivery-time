<?php
declare(strict_types=1);

namespace Linets\DeliveryTime\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface DeliveryTimeRepositoryInterface
{

    /**
     * Save deliveryTime
     * @param \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface $deliveryTime
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface $deliveryTime
    );

    /**
     * Retrieve deliveryTime
     * @param string $deliverytimeId
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($deliverytimeId);

    /**
     * Retrieve deliveryTime matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Linets\DeliveryTime\Api\Data\DeliveryTimeSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete deliveryTime
     * @param \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface $deliveryTime
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Linets\DeliveryTime\Api\Data\DeliveryTimeInterface $deliveryTime
    );

    /**
     * Delete deliveryTime by ID
     * @param string $deliverytimeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($deliverytimeId);
}

