<?php

namespace Linets\DeliveryTime\Plugin\Checkout;

class SaveAddressInformation
{
    protected $quoteRepository;

    public function __construct(
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Checkout\Model\ShippingInformationManagement $subject
     * @param $cartId
     * @param \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
     */
    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $shippingAddress = $addressInformation->getShippingAddress();
        $shippingAddressExtensionAttributes = $shippingAddress->getExtensionAttributes();
        $avaibleDays = '';
        $promise = '';

        if ($shippingAddressExtensionAttributes) {
            $avaibleDays = $shippingAddressExtensionAttributes->getAvaibledays();
            $promise = $shippingAddressExtensionAttributes->getPromise();

            $shippingAddressExtensionAttributes->setAvaibledays($avaibleDays);
            $shippingAddressExtensionAttributes->setPromise($promise);

            $shippingAddress->setExtensionAttributes($shippingAddressExtensionAttributes);
        }
        $this->_checkoutSession->setAvaibledays($avaibleDays);
        $this->_checkoutSession->setPromise($promise);
    }
}
