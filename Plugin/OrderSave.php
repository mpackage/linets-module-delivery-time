<?php

namespace Linets\DeliveryTime\Plugin;

use Magento\Framework\Exception\CouldNotSaveException;

class OrderSave
{

    /**
     * @var \Linets\DeliveryTime\Helper\data
     */
    protected $_helper;

    /**
     * @var \Linets\DeliveryTime\Helper\PromiseFactory
     */
    protected $_promiseFactory;

    /**
     * OrderSave constructor.
     * @param \Linets\DeliveryTime\Helper\Data $data
     * @param \Linets\DeliveryTime\Helper\PromiseFactory $promiseFactory
     */
    public function __construct(
        \Linets\DeliveryTime\Helper\Data $data,
        \Linets\DeliveryTime\Model\PromiseFactory $promiseFactory
    )
    {
        $this->_helper = $data;
        $this->_promiseFactory = $promiseFactory;
    }

    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    )
    {
        $resultOrder = $this->savePromiseAttribute($resultOrder);
        return $resultOrder;
    }

    private function savePromiseAttribute(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $deliveryDay = '';
        $arrive = '';
        $this->_helper->getPromise($order, $deliveryDay,$arrive);

        $promise = $this->_promiseFactory->create();
        $promise->setOrder($order->getId());
        $promise->setDays($deliveryDay);
        $promise->setDate($arrive);
        $promise->save();

        return $order;
    }
}
