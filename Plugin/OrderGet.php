<?php


namespace Linets\DeliveryTime\Plugin;

use Magento\Sales\Api\Data\OrderExtensionFactory;
use Magento\Sales\Api\Data\OrderExtensionInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderSearchResultInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class OrderGet {

    protected $_orderExtensionFactory;

    protected $_promiseFactory;

    public function __construct(
        OrderExtensionFactory $orderExtensionFactory,
        \Linets\DeliveryTime\Model\PromiseFactory $promiseFactory
    ) {
        $this->_orderExtensionFactory = $orderExtensionFactory;
        $this->_promiseFactory = $promiseFactory;
    }

    public function afterGet(
        OrderRepositoryInterface $subject,
        OrderInterface $order
    ) {
        $extensionAttributes = $order->getExtensionAttributes();
        $orderExtension = $extensionAttributes ? $extensionAttributes : $this->_orderExtensionFactory->create();

        $collectionPromise = $this->_promiseFactory->create()->getCollection();
        $collectionPromise->addFieldToFilter('order', ['eq' => $order->getId()]);
        $promise = $collectionPromise->getFirstItem();

        $extensionAttributes->setDays($promise->getDays());
        $extensionAttributes->setDate($promise->getDate());

        $order->setExtensionAttributes($orderExtension);

        return $order;
    }

    /**
     * Add "order_comment" extension attribute to order data object to make it accessible in API data of all order list
     *
     * @return OrderSearchResultInterface
     */
    public function afterGetList(
        OrderRepositoryInterface $subject,
        OrderSearchResultInterface $searchResult
    )
    {
        $orders = $searchResult->getItems();

        foreach ($orders as &$order) {
            $collectionPromise = $this->_promiseFactory->create()->getCollection();
            $collectionPromise->addFieldToFilter('order', ['eq' => $order->getId()]);
            $promise = $collectionPromise->getFirstItem();

            $extensionAttributes = $order->getExtensionAttributes();
            $extensionAttributes = $extensionAttributes ? $extensionAttributes : $this->extensionFactory->create();

            $extensionAttributes->setDays($promise->getDays());
            $extensionAttributes->setDate($promise->getDate());

            $order->setExtensionAttributes($extensionAttributes);
        }

        return $searchResult;
    }

}
