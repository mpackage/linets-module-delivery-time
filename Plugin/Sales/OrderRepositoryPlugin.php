<?php

namespace Linets\DeliveryTime\Plugin\Sales;

class OrderRepositoryPlugin
{

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession
    ) {
        $this->_checkoutSession = $checkoutSession;
    }

    public function beforeSave(\Magento\Sales\Api\OrderRepositoryInterface $subject, \Magento\Sales\Api\Data\OrderInterface $order)
    {
        /*
        $quote = $order->getQuoteId();
        $additionalInformation = $order->getPayment()->getAdditionalInformation();
        $additionalInformation['giftwrap'] = $this->_checkoutSession->getGiftwrap();
        $order->getPayment()->setAdditionalInformation($additionalInformation);
        */
    }

}
